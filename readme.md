
Oblong is self-explanatory; just call it without arguments.

For Mac or Linux, you may have to make the file executable;
open a command shell, go to the directory where oblong-linux
or oblong-mac is, and type "chmod a+x oblong <enter>".

You can rename binaries; the program does not keep track of
its own name.



--------------------------------------
Notes for compilation


For Linux or Mac:

    gcc -lm -O3 -o oblong -DBITS64 oblong.c

    For linux, you may have to add "/usr/lib/libm.a"
    after "oblong.c" (the library containing calls to
    some math functions).

For Windows (Watcom):

    create a project, and define (under Options/C compiler switches)
    a macro "WATCOM" under "Source switches" --this is so that
    the program will use functions in the Watcom library, enabling
    it to handle larger files.


